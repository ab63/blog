---
title: Bugzilla integration in the KDE wikis
date:   2019-11-23 18:20:35 +0100
layout: post
---

The Bugzilla integration in the KDE wikis is now usable again.

KDE wikis were using a long time ago the Bugzilla integration extension
developed by Mozilla. This was used for example by some KDE components to keep track
of the feature planned for a new release.

Sadly this extension wasn't keep up to date and was not compatible with recent
MediaWiki extension.

So I developed a [new extension](https://invent.kde.org/websites/mediawiki-bugzilla).
This doesn't have all the features of the old extension but all the one needed for KDE.

The extension can be see in use at [community.kde.org/Schedules/Applications/19.12_Feature_Plan](https://community.kde.org/Schedules/Applications/19.12_Feature_Plan).

```html
<bugzilla title="TODO" class="table-bugs-todo">
{
  "product": "umbrello",
  "status" : "CONFIRMED",
  "severity" : "wishlist",
  "f2": "target_milestone",
  "o2": "substring",
  "v2": "19.12",
  "include_fields" : [ "id", "product", "summary", "assigned_to"]
}
</bugzilla>
```

And this should display a list of all open which list for the milestone 19.12.

Enjoy :)
