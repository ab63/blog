---
layout: post
title:  "The new userbase wiki"
date:   2019-07-12 11:35:35 +0200
lang: en
author: "Carl Schwan"
description: "KDE userbase, the wiki where users can learn about everything KDE, gets a make over: cleaner, clearer and easier to use."
---

I'm happy to announce that the userbase wiki is getting a new theme and an updated
MediaWiki version.

## New theme - Aether

The old userbase theme was called Neverland and looked a bit antiquated. A new theme
was created with a similar look to [kde.org](https://kde.org).

The new theme features a light and dark modes using the new
`prefers-color-scheme: dark` CSS media query. The new theme is also mobile friendly.

![mediawiki old](/assets/img/mediawiki-dark.png)

![mediawiki old](/assets/img/mediawiki-default.png)

I think this is quite an improvement over this:

![mediawiki old](/assets/img/mediawiki-old.png)

I am confident that [Claus_Chr](https://userbase.kde.org/User:Claus_chr) and me found
most of the visual glitches, but if you do find a glitch, please report it to me on my
[talk page](https://userbase.kde.org/User_talk:Ognarb).

The new theme is hosted in KDE
[gitlab instance](https://invent.kde.org/websites/aether-mediawiki). Contributions
are welcome.

## New MediaWiki version

We jumped MediaWiki from the obsolete version 1.26 to 1.31, the latest LTS version. This
should fix some of the long-standing bugs and allow us to get all security updates with
minimal maintenance needs.

## What's next?

A similar update for the community and techbase wikis should be comming soon™. The only
thing that we still need to work is an update of the configuration files and some testing
to make sure nothing broke during the update. A preview version of the community wiki can
already be tested at: [wikisandbox.kde.org](https://wikisandbox.kde.org).

## Contribute to Userbase

When you find a kool feature in KDE software, you can write a small tutorial or just
a small paragraph about it and the [KDE Userbase Wiki](https://userbase.kde.org) is
the right place to publish it. You don't need to know how to code, have perfect English
or know how MediaWiki's formatting work, to contribute. We also need translators.

![I want you for userbase](/assets/img/i-want-you-userbase.png)

Admire my GIMP skills ;)

Thanks to [Blumen Herzenschein](https://gitlab.com/ognarb/blog/merge_requests/2) and
[Paul Brown](https://gitlab.com/ognarb/blog/merge_requests/3) for proofreading this blog
post and to [Ben Cooksley](https://phabricator.kde.org/p/bcooksley/) for pointing me to
the right direction.

Discussion: [Reddit](https://www.reddit.com/r/kde/comments/ccnogd/the_new_userbase_wiki/) or
[Mastodon](https://linuxrocks.online/@carl/102433547285904922)

<style>
img, video {
    max-width: 100%;
    margin-left: auto;
    margin-right: auto;
    display: block;
}
</style>
