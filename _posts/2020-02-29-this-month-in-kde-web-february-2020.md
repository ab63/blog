---
layout: post
title:  "This month in KDE Web: January-February 2020"
date:   2020-02-29 18:00:00 +0200
---

This is the first post in a monthly series about improvements to the KDE
websites. I plan to publish it every last Saturday of the month. Since a
lot happened in January and I didn't mention it anywhere, I will also
mention those things in this post.

## New Websites

* [KDE Connect](https://kdeconnect.kde.org) gained a new website
and a promotional video. This was done during the **Season of KDE** project.
(Arjun Thekoot Harisankar, [see repository](invent.kde.org/websites/kdeconnect-kde-org)).

![KDE Connect website](/assets/img/kde-connect-web.png){:.img-border}

## Updated Websites

* [Juk](https://juk.kde.org) is a music player and music manager. A new website for it
was created by Anuj Bansal, ([see commit](https://cgit.kde.org/websites/juk-kde-org.git/commit/?id=397eb218cdcad9c14e912bd7262f81a3dcc6f638)).

![Juk website](/assets/img/juk-web.png){:.img-border}

* [KDE e.V.](https://ev.kde.org) is the non-profit organization that represents
the KDE community in legal and financial matters. The e.V. website was migrated
to Jekyll and now uses the shared [Jekyll theme](https://invent.kde.org/websites/jekyll-jde-theme).
(Me: Carl Schwan, [see commit](https://cgit.kde.org/websites/ev-kde-org.git/log/)).

![KDE e.V. website](/assets/img/ev-web.png){:.img-border}

* [KMyMoney](https://kmymoney.org) is a personal finance manager and the project
website was also migrated to Jekyll and now uses the shared Jekyll theme (Me:
Carl Schwan, [see commit](https://cgit.kde.org/websites/kmymoney-org.git/commit/?id=a27d3784f92806fd04c6d6968c782b27ed042bac)).

![KMyMoney website](/assets/img/kmymoney-web.png){:.img-border}

* [kde-china.org](https://kde-china.org) is now using the Jekyll theme and is
hosted in the KDE infrastructure (Yunhe Guo, [see repository](https://invent.kde.org/websites/kde-china-org)).

![KDE chinal website](/assets/img/kde-china-web.png){:.img-border}

## Updates to kde.org

* A [hardware page](https://kde.org/hardware) was created listing all the
devices that ship with Plasma preinstalled (Niccolò Venerandi, [see commit](https://phabricator.kde.org/R883:1561070)).

* Updated image for the recently launched Plasma 5.18 (Jonathan Riddell and Me: Carl
Schwan).

* Reduce KDE patron logo size for kde.org (Ilya Bizyaev, [see task](https://phabricator.kde.org/T11878)).

## Library updates

* Added options to customize footer in the Jekyll theme. This will be helpful for
the coming update to [jp.kde.org](https://jp.kde.org) (Jumpei Ogawa,
[see commit](https://invent.kde.org/websites/jekyll-kde-theme/commit/8c7432271a9063349e7f1f0c0681d225e71fd2c5)).

* Added option to disabled the donation form in the Jekyll theme. (Jumpei Ogawa,
[see commit](https://invent.kde.org/websites/jekyll-kde-theme/commit/ed17dfd74a9f31c53012f08a26ceb04e2e8fe444)).

* Various minor fixes to the [Aether css theme](https://invent.kde.org/websites/aether-sass)
(Anuj Bansal and Me: Carl Schwan).

* A component selector for the Jekyll theme was also added. This will be helpful
for making kontact.kde.org use the KDE Jekyll theme and not a custom theme and for
others projects who have multiples components (e.g. Calligra). (Anuj Bansal,
[see merge request](https://invent.kde.org/websites/jekyll-kde-theme/merge_requests/13)).

* Improve translations infrastructure for [kate-editor.org](https://kate-editor.org)
(Me: Carl Schwan,
[see commits](https://invent.kde.org/websites/kate-editor-org/-/commit/9bd5b9eda7adfad0690eb485b935ba7ae6a5537c)).

## How you can help

We always need help with website, fixing papercuts, upgrading old
websites to the new Jekyll/Hugo infrastructure, making sure information on
the website is up-to-date, creating beautiful home page for your
favorite project and a lot more.

The Elisa maintainers are looking for someone who wants to create a website and
a [junior job task was created](https://phabricator.kde.org/T12726).

You can join the web team through our [Matrix channel](https://webchat.kde.org/#/room/#freenode_#kde-www:matrix.org)
, our IRC channel (#kde-www) or our [Telegram channel](https://t.me/KDEWeb).

<!--

Post about this next month after deployment

## Comming soon

* A new website for the Calligra project was developed during SoK by Anuj
Bansal. It still needs to be reviewed by the Calligra maintainers before
landing it.

* A new website for the Umbrello project was developed during SoK by Akshay
Nair. It also needs to be reviewed by the Umbrello maintainers before landing
it.
-->
