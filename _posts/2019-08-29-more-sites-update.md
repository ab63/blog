---
layout: post
title:  "KDE websites infrastructure update and new websites"
date:   2019-08-29 11:35:35 +0200
lang: en
author: "Carl Schwan"
description: ""
---

Since my latest [post](/2019/07/12/new-userbase.html) two months ago,
a lot of things changed regarding the KDE websites. More and more KDE websites
are switching to the Aether theme designed by [Ken Vermette](https://kver.wordpress.com/).
You can follow the progress at the [Phabricator task T10827](https://phabricator.kde.org/T10827).

## New websites

* All three wikis were updated to MediaWiki 1.31 and are now using the
[Aether MediaWiki theme](https://invent.kde.org/websites/aether-mediawiki/). There are some
visual glitches when using the dark theme version that can still be observed. They are usually very
simple to fix, so please report them in [bugs.kde.org](https://bugs.kde.org/enter_bug.cgi?product=KDE%20MediaWiki).

![All 3 kde wiki](/assets/img/wikiupdate.svg)

* The French speaking KDE websites was also updated to use the
[Aether Jekyll theme](https://invent.kde.org/websites/jekyll-kde-theme). It's only an aesthetic
change and some parts of the content still need to be updated. If you speak French and want to
help, contact the French speaking KDE community in #kde-fr (IRC/Matrix) or
[via email](https://mail.kde.org/mailman/listinfo/kde-francophone).

![Screenshot fr.kde.org](/assets/img/fr.kde.org.png)

* [Choqok](https://choqok.kde.org) also got a new website.

![Screenshot choqok.kde.org](/assets/img/choqok.kde.org.png)

* The [wiki explanation page](https://wiki.kde.org) was also
[updated](https://cgit.kde.org/websites/wiki-kde-org.git/commit/?id=1292cff44980b39417f4b51b6d9830bf3333dea7).

## Behind the scenes

### Using a single codebase for the CSS theming

One of the big problems encountered was the multiplication of different versions of
the CSS files. There is a CCS file used by [konsole.kde.org](https://konsole.kde.org)
and [choqok.kde.org](https://choqok.kde.org), one for all the MediaWiki
instances, and one for [kde.org](https://kde.org). This was getting harder
and harder to maintain, so I decided to create a single SASS codebase for all
the KDE websites.

The code is located in the [KDE Gitlab](https://invent.kde.org/websites/aether-sass)
instance and uses [Symfony Encore](https://symfony.com/doc/current/frontend.html) to
generate all the CSS files from the SASS codebase.

For the moment, the CSS code is only split into multiple SASS modules and the
tooling builds multiple versions using some generic components (breeze buttons) and other
more specific components (MediaWiki dark theme).

Compiling the SASS files to CSS is done using
[the KDE Binary Factory](https://invent.kde.org/sysadmin/binary-factory-tooling/blob/master/staticweb/pipeline-templates/symfonyencore.pipeline)
and produces two versions of each file, one with versioning (e.g. boostrap.hi8yh2huh.css)
which is intended for all the dynamics websites (MediaWiki, kde.org, ...) and another one without
versioning for the static websites (Hugo and Jekyll).

### Using versioned assets in a MediaWiki skin

MediaWiki uses ResourceLoader to load assets, but in our case we want to use the Webpack
generated `manifest.json` to load the versioned file. For this, I'm using a Symfony
component [asset](https://symfony.com/doc/current/components/asset.html) that is doing
most of the job for me.

Here is the code used:

```php

$urlPackage = new UrlPackage(
    'https://cdn.kde.org/',
    new JsonManifestVersionStrategy('https://cdn.kde.org/aether-devel/version/manifest.json')
);

$out->addStyle( $urlPackage->getUrl('aether-devel/version/bootstrap.css'), 'all' );
```

I actually created a fork of the JsonManifestVersionStrategy class because of a [bug](https://github.com/symfony/symfony/issues/33001)
in the library. I hope my change will be merged upstream.

### New features in the Jekyll theme

The Jekyll theme got some nice new features added by various contributors.

* Eike Hein added support for [meta tags allowing some basic SEO from page metadata](https://invent.kde.org/websites/jekyll-kde-theme/merge_requests/1)
* Jumpei Ogawa added support for [a custom navigation menu](https://invent.kde.org/websites/jekyll-kde-theme/merge_requests/3)
* I added a [blog component](https://invent.kde.org/websites/jekyll-kde-theme/merge_requests/4)

And special thanks to Mark Winter for fixing a small typo.

## Junior Job

There are multiple other websites in the pipeline and any help is welcome.

* [Update RKward website](https://phabricator.kde.org/T11404)
* [Update KMyMoney website](https://phabricator.kde.org/T11403)
* [Update Umbrello website](https://phabricator.kde.org/T11466) I already started it, but if
someone wants to take care of it, they are welcome.
* [Update kde.org/plasma-desktop](https://phabricator.kde.org/T11243) There are still some
screenshots and videos missing.

If you are interested in one of this tasks, join us in [#kde-www](https://community.kde.org/Matrix) in IRC/Matrix.

## Websites Bof at Akademy

I'm going to Akademy and I will organize a BoF about the websites. It will take place
Tuesday, 10th September 2019 in room U2-08b, just after the Plasma BoFs.

If you are interested and want to attend, you can put yourself in the list
[T11423](https://phabricator.kde.org/T11423).

<style>
.post-content img {border: 1px #808080 solid; }
</style>

