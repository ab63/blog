---
layout: post
title:  "New webpage for Plasma Desktop"
date:   2019-09-16 09:35:35 +0200
lang: en
author: "Carl Schwan"
description: "Story about the new webpage for Plasma Desktop"
---

In my quest to improve the website of KDE, I updated the [Plasma Desktop webpage](https://kde.org/plasma-desktop).
This is a huge improvement to the old website, which didn't show any screenshots
and didn't list any Plasma features.

I already teased the improvements I made in the Plasma BoF in Milan to the Akademy.

![Me (Carl Schwan) at the Plasma BoF showing the new Plasma Desktop webpage](/assets/img/plasma-bof-webpage.png)

The redesign got a lot of positive feedback by the Plasma team and after some small
modifications the changes landed.

The webpage looks like this now:

![Plasma desktop webpage](/assets/img/plasma-desktop-webpage.png)

Thanks to all the people from the [Promo team](https://community.kde.org/Promo) and
[vinz](https://less.re/profile/vinzv) who helped me write the text and give me some ideas.

## Improving the KDE websites: Junior Jobs

If you want to help improving the web presence of KDE, I regularly add some Junior Job to this
[Phabricator Workboard](https://phabricator.kde.org/tag/websites/). Lots of things need to be
updated, so don't hesitate to propose other changes in the
[kde-www mailing list](https://mail.kde.org/mailman/listinfo/kde-www).

Discussion: [Reddit](https://www.reddit.com/r/kde/comments/d50h6a/new_plasma_desktop_webpage_showcases_its_favorite/) or
[Mastodon](https://linuxrocks.online/@carl/102802358970158871)

<style>
img, video {
    max-width: 100%;
    margin-left: auto;
    margin-right: auto;
    display: block;
}
</style>
