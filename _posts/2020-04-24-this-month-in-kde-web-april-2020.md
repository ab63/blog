---
layout: post
title:  "This month in KDE Web: April 2020"
date:   2020-04-25 18:00:00 +0200
image: /assets/img/kde-org-homepage.png
locale: en
---

Like every month the KDE web developers worked hard on improving the websites.

## Updated Websites

* A sidebar design for Aether (the KDE web theme) was created. It is currently used
for [api.kde.org](https://api.kde.org) and the wikis. The next step is to move the
sidebar to the right for the wikis and port the sphinx aether theme to use it.
(Carson Black and Me: Carl Schwan).

![api.kde.org](/assets/img/api-sidebar.png)

## KDE.org changes

* The KDE.org homepage was finally updated and is now more visually appealing and
provide more information about the KDE community and the tools we create. You can
read more about in the [previous blog post](/2020/04/18/a-new-look-for-kde-org.html).
(Me: Carl Schwan and the entire promo group).

![kde.org](/assets/img/kde-org-homepage.png)

## Under the hood

* The [Kontact](https://kontact.kde.org) homepage now use the KDE Jekyll theme instead
of using a custom jekyll theme (Anuj Bansal).

## How you can help

We always need help with websites, fixing papercuts, upgrading old
websites to the new Jekyll/Hugo infrastructure, making sure information on
the websites are up-to-date, creating beautiful home page for your
favorite project and a lot more.

You can join the web team through our [Matrix channel](https://webchat.kde.org/#/room/#freenode_#kde-www:matrix.org)
, our IRC channel (#kde-www) or our [Telegram channel](https://t.me/KDEWeb).
