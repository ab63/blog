---
layout: post
title:  "New features for kde.org"
date:   2019-06-15 11:35:35 +0200
lang: en
author: "Carl Schwan"
description: "New features for kde.org"
---

In the last week, there have been various small improvements to the
[kde.org website](https://kde.org).

<style>
img, video {
    max-width: 100%;
}
</style>

## Search bar in kde.org/applications

KDE has a lot of applications and with the last update to
[kde.org/application](https://kde.org/applications) made by Jonathan
Riddell, all these applications are now displayed on the website.

The problem is that it's now difficult to search for a specific 
application, so I added a search bar to this page. See
[Merge request 5 kde-org-applications](https://invent.kde.org/websites/kde-org-applications/merge_requests/5)

![Search bar in kde.org/applications](https://invent.kde.org/websites/kde-org-applications/uploads/fd1411036f24facb9e1dda6b42bf4846/search.png)

## Support multiple screenshots per application

According to the AppStream specification, an application can have multiple
screenshots. If before only the first screenshot was displayed,
now all screenshots are displayed in a carousel. See
[Merge request 3 kde-org-applications](https://invent.kde.org/websites/kde-org-applications/merge_requests/3)

<video controls>
  <source type="video/mp4" src="/assets/simplescreenrecorder-2019-06-15_21.42.03.mp4">
</video>

## Adding schema.org information

The application page now contains some additional metadata
information. This can help search engines to better understand
the content of the webpage. See [Merge request 7 kde-org-applications](https://invent.kde.org/websites/kde-org-applications/merge_requests/7)

## Improving the Plasma product page

With Plasma 5.16, we now have a new wallpaper, so I changed the
wallpaper displayed at [kde.org/plasma-desktop](https://kde.org/plasma-desktop).
Problem: the contrast between the text and the background wasn't
great. So I added a small breeze like window, created only with
CSS and HTML.

![Window breeze like with only CSS](/assets/img/css_window.png)

## Junior jobs

There still are a lot of things that can be improved, and we would appreciate
some help to do it. If you always wanted to contribute to KDE, but
don't have any knowledge in Cpp and Qt, then you can also help
with web development.

If you are interested, I listed some junior jobs.

+ The carousel used to display the application screenshots still
has some problems. The next and previous buttons don't use the
same theme as the carousel shown on the homepage. Also, when
screenshots have a different size, the transition is not perfect.
For more information, see [bug 408728](https://bugs.kde.org/show_bug.cgi?id=408728).

+ We need to add a description for each page in kde.org. This
will improve the SEO used for KDE. First we need to define the new variable in
aether, use it, and then we can start adding descriptions.
This task needs to be done in cooperation with kde-promo and this
should help with Search Engine Optimization (SEO).

I wrote a small [installation instruction](https://community.kde.org/KDE.org/Local_Setup)
on how to setup your local development environment. And if you need any help,
you can as always contact me in Mastodon at
[@carl@linuxrocks.online](https://linuxrocks.online/@carl) or with
matrix at @carl:kde.org.
